package taras.gushul.com.googlesessions

import android.app.Activity
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.fitness.Fitness
import com.google.android.gms.fitness.FitnessOptions
import com.google.android.gms.fitness.data.DataType
import com.google.android.gms.fitness.data.Field
import com.google.android.gms.fitness.request.SessionReadRequest
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    companion object {
        const val GOOGLE_FIT_PERMISSIONS_REQUEST_CODE = 111
    }

    private lateinit var client: GoogleApiClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        signIn()
    }

    private fun signIn() {
        val fitnessOptions = FitnessOptions.builder()
                .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_WORKOUT_EXERCISE, FitnessOptions.ACCESS_READ)
                .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
                .build()
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    GOOGLE_FIT_PERMISSIONS_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions)
        } else {
            register()
        }
    }

    private fun register() {
        client = GoogleApiClient.Builder(this)
                .addApi(Fitness.SESSIONS_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        client.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onConnected(p0: Bundle?) {
        val intent = Intent(this, SessionEndBroadcastReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
        Fitness.SessionsApi.registerForSessions(client, pendingIntent)
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GOOGLE_FIT_PERMISSIONS_REQUEST_CODE) {
                register()
            }
        }
    }

    private fun readFitnessData() {
        val cal = Calendar.getInstance()
        val now = Date()
        cal.time = now
        val endTime = cal.timeInMillis
        cal.add(Calendar.WEEK_OF_YEAR, -1)
        val startTime = cal.timeInMillis

        val readRequest = SessionReadRequest.Builder()
                .setTimeInterval(startTime, endTime, TimeUnit.MILLISECONDS)
                .read(DataType.TYPE_HEART_RATE_BPM)
                .read(DataType.TYPE_DISTANCE_DELTA)
                .read(DataType.TYPE_WORKOUT_EXERCISE)
                .enableServerQueries()
                .readSessionsFromAllApps()
                .build()
        Fitness.getSessionsClient(this, GoogleSignIn.getLastSignedInAccount(this)!!)
                .readSession(readRequest)
                .addOnSuccessListener {
                    val sessions = it.sessions
                    for (session in sessions) {
                        val dataSet = it.getDataSet(session)
                        for (item in dataSet) {
                            val dataPoints = item.dataPoints
                            for (point in dataPoints) {
                                Log.i("TAG", "time = " + (point.getEndTime(TimeUnit.SECONDS) - point.getStartTime(TimeUnit.SECONDS)))
                                if (point.dataType == DataType.TYPE_DISTANCE_DELTA) {
                                    Log.i("TAG", "distance = " + point.getValue(Field.FIELD_DISTANCE))
                                }
                                if (point.dataType == DataType.TYPE_HEART_RATE_BPM) {
                                    Log.i("TAG", "BPM = " + point.getValue(Field.FIELD_BPM))
                                    tv.text = point.getValue(Field.FIELD_BPM).toString()
                                }
                            }
                        }
                    }
                }
                .addOnFailureListener {
                    Log.i("TAG", "Failed to read session")
                }
    }
}
