package taras.gushul.com.googlesessions

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

/**
 * Created: Gushul Taras
 * E-mail: gushultaras125@gmail.com
 * Company: APPGRANULA LLC
 * Date: 11.10.2018
 */
class SessionEndBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("TAG", "onReceive method")
        if (intent?.type?.contains("vnd.google.fitness.activity") == true) {
            Log.i("TAG", "onReceiver success")
            val startTime = intent.getLongExtra("vnd.google.fitness.start_time", 0)
            val endTime = intent.getLongExtra("vnd.google.fitness.end_time", 0)
            if (startTime != 0L && endTime != 0L) {
                Log.i("TAG", "END SESSION")
            } else {
                Log.i("TAG", "START SESSION")
            }
        }
    }
}